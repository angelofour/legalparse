Décret Parser README
Overview

This project involves the creation of a lexer and parser for decrees using the moo library for lexical analysis in TypeScript. The lexer and parser are designed to handle the structure and language typically found in decrees, such as titles, object descriptions, presidential names, rapporteurs, visas, and more.
Features

    Lexical Analysis: The lexer breaks down the input text into meaningful tokens such as words, numbers, punctuation, and whitespace.
    Parsing: The parser organizes these tokens into a structured format, identifying key components of a decree.
    Language Support: Handles French language constructs, including accents and special characters.

Installation

To install the necessary dependencies for this project, run:

```sh

npm install 
```
Usage

To use the lexer, first import the moo library and compile the lexer definition. Then, feed the input text to the lexer to generate tokens. These tokens can then be passed to the parser to create a structured representation of the decree.
Example

Grammar
*****



License

This project is licensed under the MIT License. See the LICENSE file for details.