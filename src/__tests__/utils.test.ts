
import * as utils from '../utils';

describe('utils', () => {
  describe('utils#getMessage()', () => {
    it('should return a message', () => {
      const message = utils.getMessage();
      expect(message).toEqual('Hello world!');
    });
  });

  describe('utils#testParagraphs()', () => {
    it('Should extract the first paragraph', () => {
      const lexer = utils.createLexer(`DECRET N° 2023-038 DU 05/04/2023
      déclarant d’utilité publique et autorisant les travaux
      de construction d’une centrale solaire photovoltaïque
      à Dapaong
      LE PRESIDENT DE LA REPUBLIQUE,
      Sur rapport conjoint du ministre de l’Economie et des Finances, du
      ministre délégué auprès du Président de la République, chargé des Mines
      et de l’Energie et du ministre d’Etat, ministre de l’Administration Territoriale,
      de la Décentralisation et du Développement des Territoires ;`);
    
      expect(lexer.next()?.value).toEqual("DECRET");
      expect(lexer.next()?.value).toEqual(" ");
      expect(lexer.next()?.value).toEqual("N°");
      expect(lexer.next()?.value).toEqual(" ");
      expect(lexer.next()?.value).toEqual("2023");
      expect(lexer.next()?.value).toEqual("-");
      expect(lexer.next()?.value).toEqual("038");
      expect(lexer.next()?.value).toEqual(" ");
      expect(lexer.next()?.value).toEqual("DU");
      expect(lexer.next()?.value).toEqual(" ");
      expect(lexer.next()?.value).toEqual("05");
      expect(lexer.next()?.value).toEqual("/");
      expect(lexer.next()?.value).toEqual("04");
      expect(lexer.next()?.value).toEqual("/");
      expect(lexer.next()?.value).toEqual("2023");
      expect(lexer.next()?.value).toEqual("\n");
      // expect(lexer.next()?.value).toEqual("05/04/2023");



    });
  });
});
