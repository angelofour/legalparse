// Generated automatically by nearley, version 2.20.1
// http://github.com/Hardmath123/nearley
// Bypasses TS6133. Allow declared but unused functions.
// @ts-ignore
function id(d: any[]): any { return d[0]; }
declare var comma: any;
declare var semicolon: any;
declare var white_space: any;
declare var newline: any;
declare var number: any;
declare var word: any;
declare var no_acronym: any;

const moo = require("moo");

let lexer = moo.compile({
    white_space : {
        match: /(?:\s+|\/\/[^\n\r]*(?:\n+\s*)?)+/,
        lineBreaks: true,
        value: (v: string) => v.replace(/\/\/[^-\n\r]?[^\n\r]*/g, '')
    },
    slash:/\//,
    hiphen:/-/,
    long_hiphen:/—/,
    comma:/,/,
    colon:/:/,
    semicolon:/;/,
    dot:/\./,
    backslash:/\\/,
    underscore:/_/,
    plus:'+',
    equals:'=',
    lparen:/\{|\(|\[|«/,
    rparen:/\}|\)|\]|»/,
    number: [
      {
        match: /[0-9]+/
      }
    ],
    no_acronym:/N°|n°/,
    word: /[\wÀ-Üà-øoù-ÿŒœ’']+/,
    newline: { match: /\n/, lineBreaks: true },
  })
;



interface NearleyToken {
  value: any;
  [key: string]: any;
};

interface NearleyLexer {
  reset: (chunk: string, info: any) => void;
  next: () => NearleyToken | undefined;
  save: () => any;
  formatError: (token: never) => string;
  has: (tokenType: string) => boolean;
};

interface NearleyRule {
  name: string;
  symbols: NearleySymbol[];
  postprocess?: (d: any[], loc?: number, reject?: {}) => any;
};

type NearleySymbol = string | { literal: any } | { test: (token: any) => boolean };

interface Grammar {
  Lexer: NearleyLexer | undefined;
  ParserRules: NearleyRule[];
  ParserStart: string;
};

const grammar: Grammar = {
  Lexer: lexer,
  ParserRules: [
    {"name": "decret", "symbols": ["Title", "Object", "President", "Rapporteurs", "Visas", "Decreteurs", "AnnonceCorps", "Corps_DateSignature", "Signataires", "Fin"]},
    {"name": "Title", "symbols": [{"literal":"DECRET"}, "__", "no_acronym", "__", "DECRET_ID", "__", {"literal":"DU"}, "__", "DATE", "_"]},
    {"name": "Object$ebnf$1", "symbols": []},
    {"name": "Object$ebnf$1$subexpression$1", "symbols": ["word", "_"]},
    {"name": "Object$ebnf$1", "symbols": ["Object$ebnf$1", "Object$ebnf$1$subexpression$1"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "Object", "symbols": ["Object$ebnf$1"]},
    {"name": "President$ebnf$1$subexpression$1", "symbols": ["_", {"literal":"TOGOLAISE"}]},
    {"name": "President$ebnf$1", "symbols": ["President$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "President$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "President", "symbols": [{"literal":"LE"}, "__", {"literal":"PRESIDENT"}, "__", {"literal":"DE"}, "__", {"literal":"LA"}, "__", {"literal":"REPUBLIQUE"}, "President$ebnf$1", "_", {"literal":","}, "_"]},
    {"name": "Rapporteurs$ebnf$1$subexpression$1", "symbols": ["__", {"literal":"le"}]},
    {"name": "Rapporteurs$ebnf$1", "symbols": ["Rapporteurs$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "Rapporteurs$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "Rapporteurs", "symbols": [{"literal":"Sur"}, "Rapporteurs$ebnf$1", "__", {"literal":"rapport"}, "__", {"literal":"conjoint"}, "__", "MINISTRES_RAPPORTEURS", "__"]},
    {"name": "Visas$ebnf$1$subexpression$1", "symbols": ["VISA"]},
    {"name": "Visas$ebnf$1", "symbols": ["Visas$ebnf$1$subexpression$1"]},
    {"name": "Visas$ebnf$1$subexpression$2", "symbols": ["VISA"]},
    {"name": "Visas$ebnf$1", "symbols": ["Visas$ebnf$1", "Visas$ebnf$1$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "Visas", "symbols": ["Visas$ebnf$1"]},
    {"name": "Decreteurs$ebnf$1", "symbols": []},
    {"name": "Decreteurs$ebnf$1$subexpression$1", "symbols": ["_", "word"]},
    {"name": "Decreteurs$ebnf$1", "symbols": ["Decreteurs$ebnf$1", "Decreteurs$ebnf$1$subexpression$1"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "Decreteurs$subexpression$1", "symbols": [{"literal":","}]},
    {"name": "Decreteurs$subexpression$1", "symbols": [{"literal":";"}]},
    {"name": "Decreteurs", "symbols": ["Decreteurs$ebnf$1", "_", "Decreteurs$subexpression$1", "_"]},
    {"name": "AnnonceCorps", "symbols": [{"literal":"DECRETE"}, "_", {"literal":":"}, "_"]},
    {"name": "Corps_DateSignature", "symbols": ["Corps", "DateSignature"]},
    {"name": "Corps_DateSignature", "symbols": ["DateSignature"]},
    {"name": "Corps$subexpression$1", "symbols": ["CHAPITRES"]},
    {"name": "Corps$subexpression$1", "symbols": ["ARTICLES"]},
    {"name": "Corps", "symbols": ["Corps$subexpression$1"]},
    {"name": "DateSignature", "symbols": [{"literal":"Fait"}, "__", {"literal":"à"}, "__", {"literal":"Lomé"}, "_", {"literal":","}, "_", {"literal":"le"}, "_", "number", "_", "word", "_", "number", "_"]},
    {"name": "Signataires$ebnf$1", "symbols": []},
    {"name": "Signataires$ebnf$1", "symbols": ["Signataires$ebnf$1", "NOM_PERSON_EL"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "Signataires", "symbols": ["Signataires$ebnf$1", "_"]},
    {"name": "Fin$ebnf$1", "symbols": [{"literal":"_"}]},
    {"name": "Fin$ebnf$1", "symbols": ["Fin$ebnf$1", {"literal":"_"}], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "Fin", "symbols": ["Fin$ebnf$1", "_"]},
    {"name": "DECRET_ID$ebnf$1$subexpression$1", "symbols": [{"literal":"/"}, "word"]},
    {"name": "DECRET_ID$ebnf$1", "symbols": ["DECRET_ID$ebnf$1$subexpression$1"], "postprocess": id},
    {"name": "DECRET_ID$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "DECRET_ID", "symbols": ["number", {"literal":"-"}, "number", "DECRET_ID$ebnf$1"]},
    {"name": "DATE", "symbols": ["number", {"literal":"/"}, "number", {"literal":"/"}, "number"]},
    {"name": "VISA$ebnf$1", "symbols": []},
    {"name": "VISA$ebnf$1", "symbols": ["VISA$ebnf$1", "VISA_EL"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "VISA$subexpression$1$subexpression$1", "symbols": ["_", {"literal":";"}]},
    {"name": "VISA$subexpression$1", "symbols": ["VISA$subexpression$1$subexpression$1"]},
    {"name": "VISA$subexpression$1", "symbols": [{"literal":";"}]},
    {"name": "VISA", "symbols": [{"literal":"Vu"}, "VISA$ebnf$1", "VISA$subexpression$1", "_"]},
    {"name": "CHAPITRES$ebnf$1", "symbols": []},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$subexpression$1", "symbols": ["word"]},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$subexpression$1", "symbols": ["number"]},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$ebnf$1", "symbols": []},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "symbols": [{"literal":" "}], "postprocess": id},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$ebnf$1$subexpression$1", "symbols": ["CHAPITRES$ebnf$1$subexpression$1$ebnf$1$subexpression$1$ebnf$1", "word"]},
    {"name": "CHAPITRES$ebnf$1$subexpression$1$ebnf$1", "symbols": ["CHAPITRES$ebnf$1$subexpression$1$ebnf$1", "CHAPITRES$ebnf$1$subexpression$1$ebnf$1$subexpression$1"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "CHAPITRES$ebnf$1$subexpression$1", "symbols": [{"literal":"CHAPITRE"}, "_", "CHAPITRES$ebnf$1$subexpression$1$subexpression$1", "_", {"literal":":"}, "CHAPITRES$ebnf$1$subexpression$1$ebnf$1", {"literal":":"}, "__", "ARTICLES"]},
    {"name": "CHAPITRES$ebnf$1", "symbols": ["CHAPITRES$ebnf$1", "CHAPITRES$ebnf$1$subexpression$1"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "CHAPITRES", "symbols": ["CHAPITRES$ebnf$1"]},
    {"name": "ARTICLES$ebnf$1", "symbols": ["ARTICLE"]},
    {"name": "ARTICLES$ebnf$1", "symbols": ["ARTICLES$ebnf$1", "ARTICLE"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "ARTICLES", "symbols": ["ARTICLES$ebnf$1"]},
    {"name": "ARTICLE$subexpression$1", "symbols": [{"literal":"Article"}, "_", {"literal":"premier"}]},
    {"name": "ARTICLE$subexpression$1", "symbols": [{"literal":"Art"}, {"literal":"."}, "_", "number"]},
    {"name": "ARTICLE$subexpression$2$ebnf$1", "symbols": []},
    {"name": "ARTICLE$subexpression$2$ebnf$1", "symbols": ["ARTICLE$subexpression$2$ebnf$1", "ARTICLE_EL"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "ARTICLE$subexpression$2", "symbols": ["ARTICLE$subexpression$2$ebnf$1"]},
    {"name": "ARTICLE", "symbols": ["ARTICLE$subexpression$1", "__", {"literal":":"}, "ARTICLE$subexpression$2", {"literal":"."}, "_"]},
    {"name": "FONCTION$subexpression$1", "symbols": ["PRESIDENT"]},
    {"name": "FONCTION$subexpression$1", "symbols": ["PREMIER_MINISTRE"]},
    {"name": "FONCTION$subexpression$1", "symbols": ["AUTRES_MINISTRES"]},
    {"name": "FONCTION", "symbols": [{"literal":"Le"}, "_", "FONCTION$subexpression$1"]},
    {"name": "PERSONNE$ebnf$1", "symbols": []},
    {"name": "PERSONNE$ebnf$1", "symbols": ["PERSONNE$ebnf$1", "NOM_PERSON_EL"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "PERSONNE", "symbols": ["PERSONNE$ebnf$1", "_"]},
    {"name": "PRESIDENT", "symbols": [{"literal":"Président"}, "_", {"literal":"de"}, "_", {"literal":"la"}, "_", {"literal":"République"}]},
    {"name": "MINISTRES_RAPPORTEURS$subexpression$1", "symbols": [{"literal":"DU"}]},
    {"name": "MINISTRES_RAPPORTEURS$subexpression$1", "symbols": [{"literal":"du"}]},
    {"name": "MINISTRES_RAPPORTEURS$subexpression$1", "symbols": [{"literal":"Du"}]},
    {"name": "MINISTRES_RAPPORTEURS$ebnf$1", "symbols": []},
    {"name": "MINISTRES_RAPPORTEURS$ebnf$1$subexpression$1$subexpression$1", "symbols": ["_", "word"]},
    {"name": "MINISTRES_RAPPORTEURS$ebnf$1$subexpression$1", "symbols": ["MINISTRES_RAPPORTEURS$ebnf$1$subexpression$1$subexpression$1"]},
    {"name": "MINISTRES_RAPPORTEURS$ebnf$1$subexpression$1", "symbols": [{"literal":","}]},
    {"name": "MINISTRES_RAPPORTEURS$ebnf$1", "symbols": ["MINISTRES_RAPPORTEURS$ebnf$1", "MINISTRES_RAPPORTEURS$ebnf$1$subexpression$1"], "postprocess": (d) => d[0].concat([d[1]])},
    {"name": "MINISTRES_RAPPORTEURS", "symbols": ["MINISTRES_RAPPORTEURS$subexpression$1", "MINISTRES_RAPPORTEURS$ebnf$1", "_", {"literal":";"}]},
    {"name": "PREMIER_MINISTRE", "symbols": [{"literal":"Premier"}, "_", {"literal":"ministre"}]},
    {"name": "AUTRES_MINISTRES", "symbols": ["MINISTRE_DELEGUE"]},
    {"name": "AUTRES_MINISTRES", "symbols": ["MINISTRE_ECONOMIE"]},
    {"name": "AUTRES_MINISTRES", "symbols": ["MINISTRE_ETAT"]},
    {"name": "AUTRES_MINISTRES", "symbols": ["MINISTRE_ADMIN_TERR"]},
    {"name": "MINISTRE_DELEGUE", "symbols": [{"literal":"ministre"}, "_", {"literal":"délégué"}, "_", {"literal":"auprès"}, "_", {"literal":"du"}, "_", {"literal":"Président"}, "_", {"literal":"de"}, "_", {"literal":"la"}, "_", {"literal":"République"}, "_"]},
    {"name": "MINISTRE_ECONOMIE", "symbols": [{"literal":"ministre"}, "_", {"literal":"de"}, "_", {"literal":"l’Economie"}, "_", {"literal":"et"}, "_", {"literal":"des"}, "_", {"literal":"Finances"}]},
    {"name": "MINISTRE_ETAT", "symbols": [{"literal":"ministre"}, "_", {"literal":"d’Etat"}]},
    {"name": "MINISTRE_ADMIN_TERR", "symbols": [{"literal":"ministre"}, "_", {"literal":"de"}, "_", {"literal":"l’Administration"}, "_", {"literal":"Territoriale"}, "comma", "_", {"literal":"de"}, "_", {"literal":"la"}, "_", {"literal":"Décentralisation"}, "_", {"literal":"et"}, "_", {"literal":"du"}, "_", {"literal":"Développement"}, "_", {"literal":"des"}, "_", {"literal":"Territoires"}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": ["word"]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": ["number"]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": ["no_acronym"]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"-"}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"/"}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":","}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":":"}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"("}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":")"}]},
    {"name": "VISA_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"+"}]},
    {"name": "VISA_EL$subexpression$1", "symbols": ["_", "VISA_EL$subexpression$1$subexpression$1"]},
    {"name": "VISA_EL", "symbols": ["VISA_EL$subexpression$1"]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": ["word"]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": ["number"]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": ["no_acronym"]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"-"}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"/"}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":","}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":":"}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"("}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":")"}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":";"}]},
    {"name": "ARTICLE_EL$subexpression$1$subexpression$1", "symbols": [{"literal":"+"}]},
    {"name": "ARTICLE_EL$subexpression$1", "symbols": ["_", "ARTICLE_EL$subexpression$1$subexpression$1"]},
    {"name": "ARTICLE_EL", "symbols": ["ARTICLE_EL$subexpression$1"]},
    {"name": "NOM_PERSON_EL$subexpression$1", "symbols": ["word"]},
    {"name": "NOM_PERSON_EL$subexpression$1", "symbols": [{"literal":"-"}]},
    {"name": "NOM_PERSON_EL$subexpression$1", "symbols": [{"literal":","}]},
    {"name": "NOM_PERSON_EL$subexpression$1", "symbols": [{"literal":"."}]},
    {"name": "NOM_PERSON_EL", "symbols": ["_", "NOM_PERSON_EL$subexpression$1"]},
    {"name": "OPTIONAL_NEWLINE", "symbols": ["_"]},
    {"name": "comma", "symbols": [(lexer.has("comma") ? {type: "comma"} : comma)]},
    {"name": "semicolon", "symbols": [(lexer.has("semicolon") ? {type: "semicolon"} : semicolon)]},
    {"name": "_$ebnf$1", "symbols": [(lexer.has("white_space") ? {type: "white_space"} : white_space)], "postprocess": id},
    {"name": "_$ebnf$1", "symbols": [], "postprocess": () => null},
    {"name": "_", "symbols": ["_$ebnf$1"]},
    {"name": "__", "symbols": [(lexer.has("white_space") ? {type: "white_space"} : white_space)]},
    {"name": "newline", "symbols": [(lexer.has("newline") ? {type: "newline"} : newline)]},
    {"name": "number", "symbols": [(lexer.has("number") ? {type: "number"} : number)]},
    {"name": "word", "symbols": [(lexer.has("word") ? {type: "word"} : word)]},
    {"name": "no_acronym", "symbols": [(lexer.has("no_acronym") ? {type: "no_acronym"} : no_acronym)]}
  ],
  ParserStart: "decret",
};

export default grammar;
