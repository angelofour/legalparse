@preprocessor typescript


@{%
const moo = require("moo");

let lexer = moo.compile({
    white_space : {
        match: /(?:\s+|\/\/[^\n\r]*(?:\n+\s*)?)+/,
        lineBreaks: true,
        value: (v: string) => v.replace(/\/\/[^-\n\r]?[^\n\r]*/g, '')
    },
    slash:/\//,
    hiphen:/-/,
    long_hiphen:/—/,
    comma:/,/,
    colon:/:/,
    semicolon:/;/,
    dot:/\./,
    backslash:/\\/,
    underscore:/_/,
    plus:'+',
    equals:'=',
    lparen:/\{|\(|\[|«/,
    rparen:/\}|\)|\]|»/,
    number: [
      {
        match: /[0-9]+/
      }
    ],
    no_acronym:/N°|n°/,
    word: /[\wÀ-Üà-øoù-ÿŒœ’']+/,
    newline: { match: /\n/, lineBreaks: true },
  })
;


%}

@lexer lexer

decret -> 
            Title 
            Object 
            President 
            Rapporteurs 
            Visas 
            Decreteurs 
            AnnonceCorps 
            Corps_DateSignature
            Signataires 
            Fin 
        

# DECRET N° 2023-038 DU 05/04/2023
Title -> "DECRET"  __ no_acronym  __ DECRET_ID  __ "DU"  __ DATE _
# déclarant d’utilité publique et autorisant les travaux
# de construction d’une centrale solaire photovoltaïque
# à Dapaong
Object -> (word _):*
# LE PRESIDENT DE LA REPUBLIQUE,
President -> "LE"  __ "PRESIDENT"  __ "DE"  __ "LA"  __ "REPUBLIQUE" (_ "TOGOLAISE"):? _ "," _
# Sur rapport conjoint du ministre de l’Economie et des Finances, du
# ministre délégué auprès du Président de la République, chargé des Mines
# et de l’Energie et du ministre d’Etat, ministre de l’Administration Territoriale,
# de la Décentralisation et du Développement des Territoires ;
Rapporteurs -> "Sur" (__ "le"):?  __ "rapport"  __ "conjoint"  __ MINISTRES_RAPPORTEURS __
# Vu la constitution du 14 octobre 1992 ;
# Vu la loi n° 2014-014 du 22 octobre 2014 portant modernisation de
# l’action publique de l’Etat en faveur de l’économie ;
# Vu la loi n° 2018-005 du 14 juin 2018 portant code foncier et domanial ;
# Vu le décret n° 2012-004/PR du 29 février 2012 relatif aux attributions
# des ministres d’Etat et ministres ;
# Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
# attributions du ministre et portant organisation et fonctionnement du
# ministère de l’Economie et des Finances ;
# Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
# nomination du Premier ministre ;
# Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
# du Gouvernement, ensemble les textes qui l’ont modifié ;
Visas -> (VISA):+
# Le conseil des ministres entendu,
Decreteurs -> (_ word):* _ (","|";") _
# DECRETE :
AnnonceCorps -> "DECRETE" _ ":" _
# Article premier : Sont déclarés d’utilité publique et
# autorisés, les travaux de construction d’une centrale solaire
# photovoltaïque à Dapaong.
# Art. 2 : Les travaux prévus à l’article 1er du présent décret
# couvrent un site d’une contenance superficielle de soixantequatorze hectares soixante-six ares vingt-sept centiares
# (74ha 66a 27ca), limité :
# - au nord par le domaine de la collectivité BOUM ;
# - au sud par les propriétés des collectivités LANGO et
# YALBOUME ;
# - à l’est par la collectivité YALBOUME ;
# - et à l’ouest par le domaine de la collectivité LANGO.
# Art. 3 : Le ministre de l’Economie et des Finances est
# autorisé à prendre toutes les mesures relatives à la procédure
# d’expropriation pour cause d’utilité publique.
# Art. 4 : le ministre de l’Economie et des Finances, le ministre
# délégué auprès du Président de la République, chargé des
# mines et de l’Energie et le ministre d’Etat, ministre de
# l’Administration Territoriale et du Développement des
# Territoires sont chargés, chacun en ce qui le concerne, de
# l’exécution du présent décret qui sera publié au Journal
# Officiel de la République Togolaise.
Corps_DateSignature ->  Corps  DateSignature | DateSignature
Corps -> ( CHAPITRES | ARTICLES )
# Fait à Lomé, le 05 avril 2023
DateSignature -> "Fait" __ "à" __ "Lomé" _ "," _ "le" _ number _ word _ number _
# Le Président de la République
# Faure Essozimna GNASSINGBE

# Le Premier ministre
# Victoire S. TOMEGAH-DOGBE

# Le ministre délégué auprès du Président
# de la République, chargé de l’Energie et des Mines
# Mawunyo Mila AZIABLE

# Le ministre de l’Economie et des Finances
# Sani YAYA

# Le ministre d’Etat, ministre de l’Administration Territoriale,
# de la Décentralisation et du Développement des
# Territoires
# Payadowa BOUKPESSI
# Signataires -> (FONCTION newline PERSONNE):* _
Signataires -> NOM_PERSON_EL:* _
# _______
Fin -> "_":+ _


DECRET_ID ->  number "-"  number ("/" word):?
DATE ->  number "/"  number "/"  number
VISA -> "Vu"  VISA_EL:* (( _ ";")| ";") _
# CHAPITRE IV : DE L’HOMOLOGATION DES PROJETS ET PROGRAMMES
CHAPITRES -> ("CHAPITRE" _ (word | number) _ ":" (" ":? word):* ":" __ ARTICLES ):*
ARTICLES -> ARTICLE:+
ARTICLE -> ("Article"  _ "premier" | "Art" "."  _  number) __ ":" (ARTICLE_EL:*) "." _

FONCTION -> "Le"  _ (PRESIDENT | PREMIER_MINISTRE | AUTRES_MINISTRES)
PERSONNE -> NOM_PERSON_EL:* _

PRESIDENT -> "Président"  _ "de"  _ "la"  _ "République"
MINISTRES_RAPPORTEURS ->
    ("DU" | "du" | "Du") ((_ word) | ","):* _ ";"

PREMIER_MINISTRE -> "Premier"  _ "ministre"
AUTRES_MINISTRES ->
    MINISTRE_DELEGUE | MINISTRE_ECONOMIE | MINISTRE_ETAT | MINISTRE_ADMIN_TERR

MINISTRE_DELEGUE ->
    "ministre" _ "délégué"  _ "auprès"  _ "du"  _ 
    "Président"  _ "de"  _ "la"  _ "République"  _

MINISTRE_ECONOMIE ->
    "ministre"  _ "de"  _ "l’Economie"  _ "et"  _ "des"  _ "Finances"

MINISTRE_ETAT ->
    "ministre"  _ "d’Etat"

MINISTRE_ADMIN_TERR ->
    "ministre"  _ "de"  _ "l’Administration"  _ "Territoriale"  comma  _ "de"  _ "la"  _ "Décentralisation"  _ "et"  _ "du"  _ "Développement"  _ "des"  _ "Territoires"

VISA_EL -> (_ (word | number | no_acronym | "-" | "/" | "," | ":"| "(" | ")" | "+"))
ARTICLE_EL -> (_ (word | number | no_acronym | "-" | "/" | "," | ":"| "(" | ")" | ";" | "+"))
NOM_PERSON_EL -> _ (word  | "-" | "," | "." )


OPTIONAL_NEWLINE ->  _
# Combine repeated separators:
comma -> %comma
semicolon -> %semicolon

# Combine repeated whitespace patterns:

_ -> %white_space:? # Optional space
__ -> %white_space # Required space
newline -> %newline
number ->  %number
word ->  %word
no_acronym -> %no_acronym