
import * as nearley from "nearley";
import grammar from "./grammars/compiled/decret"
import { writeFile } from "fs";
import { json } from "stream/consumers";



let decrets = [
//     `DECRET N° 2023-038 DU 05/04/2023
// déclarant d’utilité publique et autorisant les travaux
// de construction d’une centrale solaire photovoltaïque
// à Dapaong
// LE PRESIDENT DE LA REPUBLIQUE,
// Sur rapport conjoint du ministre de l’Economie et des Finances, du
// ministre délégué auprès du Président de la République, chargé des Mines
// et de l’Energie et du ministre d’Etat, ministre de l’Administration Territoriale,
// de la Décentralisation et du Développement des Territoires ;
// Vu la constitution du 14 octobre 1992 ;
// Vu la loi n° 2014-014 du 22 octobre 2014 portant modernisation de
// l’action publique de l’Etat en faveur de l’économie ;
// Vu la loi n° 2018-005 du 14 juin 2018 portant code foncier et domanial ;
// Vu le décret n° 2012-004/PR du 29 février 2012 relatif aux attributions
// des ministres d’Etat et ministres ;
// Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
// attributions du ministre et portant organisation et fonctionnement du
// ministère de l’Economie et des Finances ;
// Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
// nomination du Premier ministre ;
// Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
// du Gouvernement, ensemble les textes qui l’ont modifié ;
// Le conseil des ministres entendu,
// DECRETE :
// Article premier : Sont déclarés d’utilité publique et
// autorisés, les travaux de construction d’une centrale solaire
// photovoltaïque à Dapaong.
// Art. 2 : Les travaux prévus à l’article 1er du présent décret
// couvrent un site d’une contenance superficielle de soixantequatorze hectares soixante-six ares vingt-sept centiares
// (74ha 66a 27ca), limité :
// - au nord par le domaine de la collectivité BOUM ;
// - au sud par les propriétés des collectivités LANGO et
// YALBOUME ;
// - à l’est par la collectivité YALBOUME ;
// - et à l’ouest par le domaine de la collectivité LANGO.
// Art. 3 : Le ministre de l’Economie et des Finances est
// autorisé à prendre toutes les mesures relatives à la procédure
// d’expropriation pour cause d’utilité publique.
// Art. 4 : le ministre de l’Economie et des Finances, le ministre
// délégué auprès du Président de la République, chargé des
// mines et de l’Energie et le ministre d’Etat, ministre de
// l’Administration Territoriale et du Développement des
// Territoires sont chargés, chacun en ce qui le concerne, de
// l’exécution du présent décret qui sera publié au Journal
// Officiel de la République Togolaise.
// Fait à Lomé, le 05 avril 2023
// Le Président de la République
// Faure Essozimna GNASSINGBE
// Le Premier ministre
// Victoire S. TOMEGAH-DOGBE
// Le ministre délégué auprès du Président
// de la République, chargé de l’Energie et des Mines
// Mawunyo Mila AZIABLE
// Le ministre de l’Economie et des Finances
// Sani YAYA
// Le ministre d’Etat, ministre de l’Administration Territoriale,
// de la Décentralisation et du Développement des
// Territoires
// Payadowa BOUKPESSI
// ______
// `,
`DECRET N° 2023-034/PR DU 15/03/2023
relatif aux mécanismes de carbone
LE PRESIDENT DE LA REPUBLIQUE TOGOLAISE,
Sur le rapport conjoint du ministre de l’Environnement et des
Ressources Forestières, du ministre de l’Administration Territoriale, de
la Décentralisation et du Développement des Territoires, du ministre du
Commerce, de l’Industrie et de la Consommation Locale, du ministre de
l’Economie et des Finances et du ministre délégué chargé de l’Energie
et des Mines;
Vu la Constitution du 14 octobre 1992 ;
Vu la Convention-cadre des Nations Unies sur les changements
climatiques du 09 mai 1992, ratifié le 08 mars 1995 ;
Vu l’Accord de Paris du 12 décembre 2015, ratifié le 28 juin 2017 ;
Vu la loi n° 2000-012 du 18 juillet 2000 relative au secteur de
l’électricité ;
Vu la loi n° 2008-005 du 30 mai 2008 portant loi-cadre sur
l’environnement ;
Vu la loi n° 2008-009 du 19 juin 2008 portant code forestier ;
Vu la loi n° 2018-010 du 08 août 2018 relative à la promotion de la
production de l’électricité à base des sources de l’énergie renouvelable ;
 
Vu le décret n° 2003-237/PR du 26 septembre 2003 relatif à la mise
en place du cadre normalisé de gestion des aires protégées ;
Vu le décret n° 2012-004/PR du 28 février 2012 relatif aux attributions
des ministres d’Etat et ministres ;
Vu le décret n° 2016-007/PR du 25 janvier 2016 relatif aux organes
de gestion de la réduction des émissions de gaz à effet de serre dues
à la déforestation et à la dégradation des forêts (REDD+) au Togo ;
Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
attributions du ministre et portant organisation et fonctionnement du
ministère de l’Economie et des Finances ;
Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
nomination du Premier ministre ;
Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
du gouvernement, ensemble les textes qui l’ont modifié ;
Le conseil des ministres entendu ;
DECRETE :
CHAPITRE Ier: DES DISPOSITIONS GENERALES
Article premier : Le présent décret fixe les règles relatives
aux mécanismes de carbone dans le cadre de la mise en
œuvre des activités d’atténuation des émissions de gaz à
effet de serre.
Art. 2 : Les dispositions du présent décret s’appliquent aux
activités qui permettent de générer les résultats sous forme
de crédit carbone, d’unité de réduction et de paiements dans
le cadre de la réduction des émissions de gaz à effet de
serre.
Le ministre chargé des changements climatiques, en
concertation avec les ministères concernés, définit par arrêté
les secteurs prioritaires et les activités éligibles au sens du
présent décret.
Art. 3 : Au sens du présent décret, on entend par :
- Activités d’atténuation : toutes les activités qui visent à
séquestrer le carbone, à réduire ou à éviter les émissions
de gaz à effet de serre ;
- Additionnalité : processus prouvant que les activités du
projet vont plus loin que les pratiques courantes et vont
permettre de réduire ou de séquestrer des émissions de
gaz à effet de serre en plus par rapport à la situation de
référence ;
- Atténuation : intervention humaine pour réduire les sources
d’émission ou augmenter les puits de gaz à effet de serre ;
- Autorité nationale de gestion des mécanismes de
carbone : autorité chargée, au niveau national, de
l’inscription, de la validation et du contrôle des projets ou
programmes de réduction des émissions de gaz à effet de
serre ;
- Contributions déterminées au niveau national :
document de planification indiquant l’engagement du pays
à contribuer aux efforts d’atténuation des émissions de gaz
à effet de serre, et de renforcement de la résilience des
communautés et des écosystèmes face aux changements
climatiques en vue de l’atteinte des objectifs de la conventioncadre des Nations Unies sur les changements climatiques
et de l’Accord de Paris ;
- Crédit carbone : tous les droits, titres et intérêts associés
aux réductions d’émission/absorption quantifiée selon
l’étalon « tonne équivalent carbone» de volume de gaz émis
(tC02eq) ;
- Gaz à effet de serre : constituants gazeux de
l’atmosphère, tant naturels qu’anthropiques, qui absorbent
le rayonnement infrarouge de la surface terrestre et les
réémettent contribuant ainsi au réchauffement de la
planète ;
- Homologation : procédure par laquelle l’autorité nationale
de gestion des mécanismes de carbone effectue un contrôle
de conformité et approuve un projet ;
- Mécanismes de carbone : ensemble des instruments
développés par les parties à la Convention-cadre des Nations
Unies sur les changements climatiques pour accompagner
les pays à réaliser leurs objectifs d’atténuation des émissions
des gaz à effet de serre et à promouvoir le développement
durable ;
- Mécanisme de gestion des plaintes : processus et
dispositif effectifs, accessibles, transparents, respectueux
de la culture locale et équitable pour résoudre à l’amiable
les plaintes liées à la mise en œuvre des projets et
programmes d’atténuation ;
- Marché de carbone régulé : marché fixant une limite
aux émissions de gaz à effet de serre et permettant les
échanges de quotas d’émissions conformément aux plans
de réduction d’émissions de carbone issus d’accords
internationaux et régionaux ;
- Marché volontaire du carbone : émission, achat et vente
de crédits carbone, sur une base volontaire ;
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 5
- Paiements : revenus issus des réductions d’émissions
certifiées liées à la mise en œuvre des activités d’atténuation ;
- Porteur de projet ou programme : toute personne
physique ou morale de droit public ou privé, nationale ou
étrangère ainsi que toute communauté locale qui entreprend
une activité d’atténuation des émissions de gaz à effet de
serre ;
- Projet d’atténuation : ensemble d’activités visant à
séquestrer le carbone, à réduire ou à éviter les émissions
de gaz à effet de serre ;
- Programme d’atténuation : ensemble de projets ayant
un même porteur et dont au moins une des activités contribue
à l’atténuation ;
- Puits de carbone ou de gaz à effet de serre : tout
processus permettant, par un mécanisme naturel ou artificiel,
de capter les gaz à effet de serre de l’atmosphère soit pour
un stockage à long terme ou une destruction ;
- REDD+ : réduction des émissions de gaz à effet de serre
dues à la déforestation et à la dégradation des forêts
associées à la gestion durable des forêts, la conservation
et l’amélioration des stocks de carbone forestier. C’est un
mécanisme des Nations Unies permettant de rémunérer les
pays pour leurs efforts de non déforestation dans le cadre
de la réduction des émissions de gaz à effet de serre dues
à la déforestation et à la dégradation des forêts ;
- Registre national : répertoire tenu par l’autorité nationale
de gestion des mécanismes de carbone destiné à recevoir
les informations liées à la procédure d’homologation des
projets d’atténuation ;
- Séquestration du carbone : captage et stockage à long
terme du carbone hors de l’atmosphère ;
- Services environnementaux : ensemble des services
d’approvisionnement fondés sur les ressources naturelles,
des services de régulation, des services de soutien à
l’économie, des services pour le bien-être et les patrimoines
culturels ;
- Sources d’émission des gaz à effet de serre : toutes
activités susceptibles de provoquer des émissions des gaz
à effet de serre ;
- Unité de réduction : crédit transmissible et négociable,
qui est inscrit au compte des émissions d’une personne
morale ou physique, après constatation d’une diminution
de ses émissions de gaz à effet de serre.
CHAPITRE Il : DE LA GOUVERNANCE DES
MECANISMES DE CARBONE
Art. 4 : La gouvernance des mécanismes de carbone est
définie par arrêté conjoint du ministre chargé des
changements climatiques et du ministre chargé des finances.
Toutefois, les entités ci - après assurent à titre transitoire la
gouvernance des mécanismes de carbone :
- l’autorité nationale de gestion des mécanismes de
carbone ;
- un comité national d’homologation des projets et de
programmes ;
- un secrétariat technique.
Art. 5 : Le ministère chargé des changements climatiques
est l’autorité nationale de gestion des mécanismes de
carbone.
Art. 6 : L’autorité nationale de gestion des mécanismes de
carbone a pour missions de :
- définir les priorités nationales en matière de
développement résilient aux changements climatiques
et à faible émission de carbone ;
- délivrer les autorisations et les lettres de non objection
aux porteurs de projets ;
- approuver les résultats d’atténuation ;
- mettre en place le registre national des projets carbone
développés ;
- représenter l’Etat auprès des entités et promoteurs de
projets d’atténuation de gaz à effet de serre et projets
d’adaptation avec co-bénéfices en atténuation ;
- suivre l’évolution des règles, modalités et procédures
des standards internationaux de certification carbone ;
- gérer le processus d’autorisation de l’utilisation des
résultats d’atténuation transférés au niveau international
et des ajustements correspondants ;
- recevoir et gérer les plaintes et recours ainsi que leur
résolution.
Art. 7 : Le comité national d’homologation est placé sous la
tutelle du ministère chargé des changements climatiques.
Il est chargé de :
- assurer une mise en œuvre des projets conforme à la
législation nationale, aux objectifs de développement
durable du pays et à ses contributions déterminées au
niveau national ;
- vérifier les lignes de base et l’additionnalité des projets ;
- donner les avis techniques sur les demandes
d’homologation des projets et programmes d’atténuation
des émissions de gaz à effet de serre.
Art. 8 : Le comité national d’homologation est composé
comme suit :
- un (1) représentant du ministère chargé des changements
climatiques, président ;
- un (1) représentant du ministère chargé des énergies,
rapporteur ;
- un (1) représentant du ministère chargé des Finances,
membre ;
- un (1) représentant du ministère chargé de l’Agriculture,
membre ;
- un (1) représentant du ministère chargé de
l’Administration territoriale, membre ;
- un (1) représentant du ministère chargé de la
Planification du Développement, membre ;
- un (1) représentant du ministre chargé du foncier,
membre ;
- un (1) représentant du ministère chargé de l’Industrie,
membre ;
- un (1) représentant du ministère des Investissement,
membre ;
- un (1) représentant du ministère chargé des transports,
membre; un (1) représentant de la faitière des
communes du Togo, membre ;
- un (1) représentant du réseau des ONG intervenant dans
le domaine de l’environnement, membre.
Le comité national peut faire appel, à titre consultatif, à toute
personne dont les compétences sont jugées nécessaires
pour l’accomplissement de ses missions.
Les modalités de fonctionnement du comité national sont
précisées par arrêté conjoint du ministre chargé de
l’environnement et du ministre chargé des Finances.
Art. 9 : Le secrétariat technique de gestion des mécanismes
de carbone est assuré par la direction de l’environnement.
Le secrétariat technique de gestion des mécanismes de
carbone peut faire appel, à toute personne dont les
compétences sont jugées nécessaires pour
l’accomplissement de ses missions.
Art. 10 : Les missions du secrétariat technique de gestion
des mécanismes de carbone sont les suivantes :
- recevoir les demandes d’enregistrement de projets
carbone de la part des promoteurs de projets ;
- évaluer les projets et les programmes d’attenuation ;
- suivre la mise en œuvre des projets et programmes ;
- organiser les réunions du comité national d’homologation ;
- élaborer les rapports nationaux ;
- gérer le système d’information sur les sauvegardes socioenvironnementales ;
- recevoir les plaintes et recours ;
- formuler des propositions au comité national
d’homologation ;
- gérer le registre national des projets et des programmes.
CHAPITRE III : DES DROITS SUR LES RESULTATS
D’ATTENUATION
Art. 11 : Le droit de générer et de disposer des résultats
sous forme de crédit carbone, d’unité de réduction et de
paiements est reconnu aux personnes physiques ou morales.
Art. 12 : Les détenteurs de crédits carbones et des unités
de réduction peuvent librement les céder ou les transférer
par voie de convention.
Les opérations de cession et de transfert sont assujetties
au paiement d’une taxe dont les modalités de recouvrement
et de gestion sont fixées par arrêté conjoint du ministre
chargé des finances et du ministre chargé des changements
climatiques.
Art. 13 : Les résultats d’atténuation liés aux marchés régulés
ou volontaires font l’objet d’une retenue par l’autorité nationale
en vue de répondre aux engagements vis-à- vis des
contributions déterminées au niveau national.
Les modalités de la retenue sont fixées par arrêté du ministre
chargé des changements climatiques.
6 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
Le taux de la retenue est fonction de l’ampleur et de la nature
du projet ou du programme
CHAPITRE IV : DE L’HOMOLOGATION DES PROJETS
ET PROGRAMMES
Art. 14 : Pour être homologués, les projets et programmes
doivent répondre aux critères d’éligibilité ci-après :
- présenter des potentialités d’atténuation des émissions
de gaz à effet de serre et un potentiel de renforcement
des capacités et de transfert de technologie ;
- être accompagné d’un plan d’investissement cohérent ;
- être aligné sur les secteurs et les activités éligibles,
définis par l’arrêté prévu à l’article 2 du présent décret ;
- prévoir des mesures de sauvegarde environnementale et
sociale, y compris le genre ;
- présenter un dispositif de suivi-évaluation qui s’intègre
au système national de mesures, notification et
vérification.
Tout projet ou programme répondant aux critères ci-dessus
mentionnés est éligible et après le contrôle, donne droit à
une autorisation délivrée par l’autorité nationale.
Art. 15 : Aux fins d’application des dispositions de l’article 9
du présent décret, il est institué un registre national des
projets et programmes d’atténuation des émissions de gaz
à effet de serre, tenu et géré par le secrétariat technique.
Un arrêté du ministre chargé de l’environnement précise les
modalités de gestion du registre national des projets et
programmes d’atténuation des émissions de gaz à effet de
serre.
Art. 16 : Les pièces constitutives du dossier de demande et
les critères d’homologation sont définis par un manuel de
procédures approuvé par arrêté du ministre chargé des
changements climatiques.
CHAPITRE V: DES MECANISMES DE PARTAGE
DES BENEFICES ET DE GESTION DES PLAINTES
DANS LE CADRE DE REDD+
Art. 17 : Les paiements basés sur les résultats liés au
mécanisme REDD+ sont versés sur le fonds national de
l’environnement (FNE).
Les modalités de partage des paiements visés à l’alinéa
1
er du présent article sont déterminées par un arrêté
interministériel du ministre chargé des changements
climatiques, du ministre chargé de l’Administration
Territoriale et du ministre chargé des Finances.
Art. 18 : Les plaintes et les conflits liés à la mise en œuvre
des activités d’atténuation sont réglés conformément au
mécanisme de gestion des plaintes mis en place dans le
cadre de REDD+.
A défaut, le litige est soumis aux tribunaux togolais
compétents.
CHAPITRE VI : DES DISPOSITIONS FINALES
Art. 19 : Le ministre de l’Environnement et des Ressources
Forestières, le ministre d’Etat, ministre de l’Administration
Territoriale, de la Décentralisation et du Développement des
Territoires, le ministre du Commerce, de l’Industrie et de la
Consommation Locale, le ministre de l’Economie et des
Finances et le ministre délégué chargé de l’Energie et des
Mines sont chargés, chacun en ce qui le concerne, de
l’exécution du présent décret qui sera publié au Journal
Officiel de la République Togolaise.
Fait à Lomé, le 15 mars 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire S. TOMEGAH-DOGBE
Le ministre de l’Administration Territoriale, de la
Décentralisation et du Développement des Territoires
Payadowa BOUKPESSI
Le ministre de l’Environnement et des Ressources
 Forestières
Katari FOLI BAZI
Le ministre de l’Economie et des Finances
Sani YAYA
Le ministre du Commerce, de l’Industrie
et de la Consommation Locale
S- T Kodjo ADEDZE
Le ministre délégué chargé de l’Energie et des Mines
Mawunyo Mila AZIABLE`


]
const journal = `JOURNAL OFFICIEL
DE LA REPUBLIQUE TOGOLAISE
PARAISSANT LE 1er ET LE 16 DE CHAQUE MOIS A LOME
DIRECTION, REDACTION ET ADMINISTRATION
CABINET DU PRESIDENT DE LA REPUBLIQUE - TEL. : 22 21 27 01 - LOME
ACHAT ABONNEMENT ANNUEL
 TOGO.............................. 20 000 F
 AFRIQUE........................ 28 000 F
 HORS AFRIQUE ........... 40 000 F
 Récépissé de déclaration d’associations .... 10 000 F
 Avis de perte de titre foncier (1er et 2e
insertions) ................................................ 20 000 F
 Avis d’immatriculation .................................... 10 000 F
 Certification du JO ............................................ 500 F
NB. : Le paiement à l’avance est la seule garantie pour être bien servi.
Pour tout renseignement complémentaire, s’adresser à l’EDITOGO Tél. : (228) 22 21 37 18 / 22 21 61 07 / 08 Fax (228) 22 22 14 89 - BP: 891 - LOME
T A R I F
ANNONCES
 1 à 12 pages.................. 200 F
 16 à 28 pages ................. 600 F
 32 à 44 pages ............... 1000 F
 48 à 60 pages ............... 1500 F
 Plus de 60 pages ......... 2 000 F
68e
 Année N° 39 bis NUMERO SPECIAL du 25 Mai 2023
2023
19 mai-Loi n° 2023-005 autorisant la ratification des statuts du
centre africain de développement minier, adoptes le 31 janvier
2016 à Addis-Abeba......................................................................
19 mai-Loi n° 2023-006 autorisant la ratification du traite portant
création de l’agence africaine nu médicament, adopte à AddisAbeba, le 11 février 2019..............................................................
19 mai-Loi n° 2023-007 autorisant la ratification de la convention
portant statut du fleuve mono et création de l’autorité du bassin du
mono, signée le 30 décembre 2014 à Cotonou..............................
19 mai-Loi n° 2023-008 autorisant l’adhésion du Togo a l’accord
sur les privilèges et immunités du tribunal international du droit de
la mer, adopte le 23 mai 1997 à New York aux USA......................
19 mai-Loi n° 2023-009 autorisant l’adhésion du Togo a la
convention internationale pour la conservation des thonidés de
l’Atlantique adoptée le 14 mai 1966 à Rio de Janeiro....................
19 mai-Loi n° 2023-010 autorisant l’adhésion du Togo a l’accord
sur la création de l’institut mondial de la croissance verte, adopte
le 20 juin 2012 à Rio de Janeiro....................................................
DECRETS
2023
15 mars-Décret n° 2023-034/PR relatif aux mécanismes de
carbone........................................................................................
05 avr.-Décret n° 2023-038 déclarant d’utilité publique et autorisant
les travaux de construction d’une centrale solaire photovoltaïque
à Dapaong...................................................................................
05 avr.-Décret n° 2023-039/PR portant création, attributions et
organisation de la Société Togolaise de Manganèse (STM)...........
ARRETES
Ministère de l’Economie et des Finances
2022
21 nov.-Arrêté n° 211/2022/MEF/UPF portant création, attributions
et organisation du Comité national d’évaluation des dépenses
fiscales........................................................................................
SOMMAIRE
PARTIE OFFICIELLE
ACTES DU GOUVERNEMENT DE LA REPUBLIQUE
TOGOLAISE
LOIS, ORDONNANCES, DECRETS, ARRETES
 ET DECISIONS
LOIS
3
3
3
8
8
11
2
2
2
3
JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023 2
DECISIONS
Cour Suprême du Togo
2023
25 mai arrêt n° 008/2023 recours N°004/R.EL/2023 du 24 mars
2023 ; Affaire : Le préfet de Tône (Tône 1) C/ quid de droit.
Présents : MM DJIDONOU : Président ; HOUSSIN, ASSAH, ZEKPA
et GBADOE Membres ; DODZRO : M.P. et DORSOU : Greffière.........
25 mai arrêt n° 009/2023 recours N°005/R.EL/2023 du 25 avril
2023 ; Affaire : le préfet de haho (haho 3) c/ quid de droit.
Présents : MM DJIDONOU : Président ; HOUSSIN, ASSAH, ZEKPA
et GBADOE : Membres ; AZANLEDJI-AHADZI : M.P. et DORSOU :
greffière.......................................................................................
PARTIE OFFICIELLE
ACTES DU GOUVERNEMENT DE LA REPUBLIQUE
TOGOLAISE
LOIS, ORDONNANCES, DECRETS, ARRETES
 ET DECISIONS
LOIS
LOI N° 2023-005 DU 19/05/2023
autorisant la ratification des statuts du centre
africain de développement minier, adoptés
le 31 janvier 2016 à Addis-Abeba
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont la teneur
suit :
Article premier : Est autorisée, la ratification des statuts
du Centre africain de développement minier, adoptés le
31 janvier 2016 à Addis-Abeba en ETHIOPIE.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
LOI N° 2023-006 DU 19/05/2023
autorisant la ratification du traite portant création
de l’agence africaine nu médicament, adopte
à Addis-Abeba, le 11 février 2019
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont la teneur
suit :
Article premier : Est autorisée, la ratification du Traité
portant création de l’Agence africaine du médicament, adopté
à Addis-Abeba, le 11 février 2019.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
————
LOI N° 2023-007 DU 19/05/2023
autorisant la ratification de la convention portant
statut du fleuve mono et création de l’autorité du
bassin du mono, signée le 30 décembre 2014
à Cotonou
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont la teneur
suit :
Article premier : Est autorisée la ratification de la
Convention portant statut du fleuve Mono et création de
l’Autorité du bassin du Mono, signée le 30 décembre 2014
à Cotonou.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
12
13
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 3
LOI N° 2023-008 DU 19/05/2023
autorisant l’adhésion du Togo a l’accord sur
les privilèges et immunités du tribunal international
du droit de la mer, adopte le 23 mai 1997
à New York aux USA
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont la teneur
suit :
Article premier : Est autorisée l’adhésion du Togo à l’Accord
sur les privilèges et immunités du Tribunal international du
droit de la mer, adopté le 23 mai 1997 à New York aux USA.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
________
LOI N° 2023-009 DU 19/05/2023
autorisant l’adhésion du Togo a la convention
internationale pour la conservation des thonidés de
l’Atlantique adoptée le 14 mai 1966 à Rio de Janeiro
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont
la teneur suit :
Article premier : Est autorisée, l’adhésion du Togo à la
Convention internationale pour la conservation des thonidés
de l’Atlantique adoptée le 14 mai 1966 à Rio de Janeiro.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
LOI N° 2023-010 DU 19/05/2023
autorisant l’adhésion du Togo a l’accord sur la
création de l’institut mondial de la croissance verte,
adopte le 20 juin 2012 à Rio de Janeiro
L’Assemblée nationale a délibéré et adopté ;
Le Président de la République promulgue la loi dont
la teneur suit :
Article premier : Est autorisée, l’adhésion du Togo à
l’Accord sur la création de l’Institut mondial de la croissance
verte, adopté le 20 juin 2012 à Rio de Janeiro.
Art. 2 : La présente loi sera exécutée comme loi de l’Etat.
Fait à Lomé, le 19 mai 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire Sidémého TOMEGAH-DOGBE
________
DECRETS
DECRET N° 2023-034/PR DU 15/03/2023
relatif aux mécanismes de carbone
LE PRESIDENT DE LA REPUBLIQUE TOGOLAISE,
Sur le rapport conjoint du ministre de l’Environnement et des
Ressources Forestières, du ministre de l’Administration Territoriale, de
la Décentralisation et du Développement des Territoires, du ministre du
Commerce, de l’Industrie et de la Consommation Locale, du ministre de
l’Economie et des Finances et du ministre délégué chargé de l’Energie
et des Mines,
Vu la Constitution du 14 octobre 1992 ;
Vu la Convention-cadre des Nations Unies sur les changements
climatiques du 09 mai 1992, ratifié le 08 mars 1995 ;
Vu l’Accord de Paris du 12 décembre 2015, ratifié le 28 juin 2017 ;
Vu la loi n° 2000-012 du 18 juillet 2000 relative au secteur de
l’électricité ;
Vu la loi n° 2008-005 du 30 mai 2008 portant loi-cadre sur
l’environnement ;
Vu la loi n° 2008-009 du 19 juin 2008 portant code forestier ;
Vu la loi n° 2018-010 du 08 août 2018 relative à la promotion de la
production de l’électricité à base des sources de l’énergie renouvelable ;
 4 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
Vu le décret n° 2003-237/PR du 26 septembre 2003 relatif à la mise
en place du cadre normalisé de gestion des aires protégées ;
Vu le décret n° 2012-004/PR du 28 février 2012 relatif aux attributions
des ministres d’Etat et ministres ;
Vu le décret n° 2016-007/PR du 25 janvier 2016 relatif aux organes
de gestion de la réduction des émissions de gaz à effet de serre dues
à la déforestation et à la dégradation des forêts (REDD+) au Togo ;
Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
attributions du ministre et portant organisation et fonctionnement du
ministère de l’Economie et des Finances ;
Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
nomination du Premier ministre ;
Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
du gouvernement, ensemble les textes qui l’ont modifié ;
Le conseil des ministres entendu ;
DECRETE :
CHAPITRE Ier: DES DISPOSITIONS GENERALES
Article premier : Le présent décret fixe les règles relatives
aux mécanismes de carbone dans le cadre de la mise en
œuvre des activités d’atténuation des émissions de gaz à
effet de serre.
Art. 2 : Les dispositions du présent décret s’appliquent aux
activités qui permettent de générer les résultats sous forme
de crédit carbone, d’unité de réduction et de paiements dans
le cadre de la réduction des émissions de gaz à effet de
serre.
Le ministre chargé des changements climatiques, en
concertation avec les ministères concernés, définit par arrêté
les secteurs prioritaires et les activités éligibles au sens du
présent décret.
Art. 3 : Au sens du présent décret, on entend par :
- Activités d’atténuation : toutes les activités qui visent à
séquestrer le carbone, à réduire ou à éviter les émissions
de gaz à effet de serre ;
- Additionnalité : processus prouvant que les activités du
projet vont plus loin que les pratiques courantes et vont
permettre de réduire ou de séquestrer des émissions de
gaz à effet de serre en plus par rapport à la situation de
référence ;
- Atténuation : intervention humaine pour réduire les sources
d’émission ou augmenter les puits de gaz à effet de serre ;
- Autorité nationale de gestion des mécanismes de
carbone : autorité chargée, au niveau national, de
l’inscription, de la validation et du contrôle des projets ou
programmes de réduction des émissions de gaz à effet de
serre ;
- Contributions déterminées au niveau national :
document de planification indiquant l’engagement du pays
à contribuer aux efforts d’atténuation des émissions de gaz
à effet de serre, et de renforcement de la résilience des
communautés et des écosystèmes face aux changements
climatiques en vue de l’atteinte des objectifs de la conventioncadre des Nations Unies sur les changements climatiques
et de l’Accord de Paris ;
- Crédit carbone : tous les droits, titres et intérêts associés
aux réductions d’émission/absorption quantifiée selon
l’étalon « tonne équivalent carbone» de volume de gaz émis
(tC02eq) ;
- Gaz à effet de serre : constituants gazeux de
l’atmosphère, tant naturels qu’anthropiques, qui absorbent
le rayonnement infrarouge de la surface terrestre et les
réémettent contribuant ainsi au réchauffement de la
planète ;
- Homologation : procédure par laquelle l’autorité nationale
de gestion des mécanismes de carbone effectue un contrôle
de conformité et approuve un projet ;
- Mécanismes de carbone : ensemble des instruments
développés par les parties à la Convention-cadre des Nations
Unies sur les changements climatiques pour accompagner
les pays à réaliser leurs objectifs d’atténuation des émissions
des gaz à effet de serre et à promouvoir le développement
durable ;
- Mécanisme de gestion des plaintes : processus et
dispositif effectifs, accessibles, transparents, respectueux
de la culture locale et équitable pour résoudre à l’amiable
les plaintes liées à la mise en œuvre des projets et
programmes d’atténuation ;
- Marché de carbone régulé : marché fixant une limite
aux émissions de gaz à effet de serre et permettant les
échanges de quotas d’émissions conformément aux plans
de réduction d’émissions de carbone issus d’accords
internationaux et régionaux ;
- Marché volontaire du carbone : émission, achat et vente
de crédits carbone, sur une base volontaire ;
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 5
- Paiements : revenus issus des réductions d’émissions
certifiées liées à la mise en œuvre des activités d’atténuation ;
- Porteur de projet ou programme : toute personne
physique ou morale de droit public ou privé, nationale ou
étrangère ainsi que toute communauté locale qui entreprend
une activité d’atténuation des émissions de gaz à effet de
serre ;
- Projet d’atténuation : ensemble d’activités visant à
séquestrer le carbone, à réduire ou à éviter les émissions
de gaz à effet de serre ;
- Programme d’atténuation : ensemble de projets ayant
un même porteur et dont au moins une des activités contribue
à l’atténuation ;
- Puits de carbone ou de gaz à effet de serre : tout
processus permettant, par un mécanisme naturel ou artificiel,
de capter les gaz à effet de serre de l’atmosphère soit pour
un stockage à long terme ou une destruction ;
- REDD+ : réduction des émissions de gaz à effet de serre
dues à la déforestation et à la dégradation des forêts
associées à la gestion durable des forêts, la conservation
et l’amélioration des stocks de carbone forestier. C’est un
mécanisme des Nations Unies permettant de rémunérer les
pays pour leurs efforts de non déforestation dans le cadre
de la réduction des émissions de gaz à effet de serre dues
à la déforestation et à la dégradation des forêts ;
- Registre national : répertoire tenu par l’autorité nationale
de gestion des mécanismes de carbone destiné à recevoir
les informations liées à la procédure d’homologation des
projets d’atténuation ;
- Séquestration du carbone : captage et stockage à long
terme du carbone hors de l’atmosphère ;
- Services environnementaux : ensemble des services
d’approvisionnement fondés sur les ressources naturelles,
des services de régulation, des services de soutien à
l’économie, des services pour le bien-être et les patrimoines
culturels ;
- Sources d’émission des gaz à effet de serre : toutes
activités susceptibles de provoquer des émissions des gaz
à effet de serre ;
- Unité de réduction : crédit transmissible et négociable,
qui est inscrit au compte des émissions d’une personne
morale ou physique, après constatation d’une diminution
de ses émissions de gaz à effet de serre.
CHAPITRE Il : DE LA GOUVERNANCE DES
MECANISMES DE CARBONE
Art. 4 : La gouvernance des mécanismes de carbone est
définie par arrêté conjoint du ministre chargé des
changements climatiques et du ministre chargé des finances.
Toutefois, les entités ci - après assurent à titre transitoire la
gouvernance des mécanismes de carbone :
- l’autorité nationale de gestion des mécanismes de
carbone ;
- un comité national d’homologation des projets et de
programmes ;
- un secrétariat technique.
Art. 5 : Le ministère chargé des changements climatiques
est l’autorité nationale de gestion des mécanismes de
carbone.
Art. 6 : L’autorité nationale de gestion des mécanismes de
carbone a pour missions de :
- définir les priorités nationales en matière de
développement résilient aux changements climatiques
et à faible émission de carbone ;
- délivrer les autorisations et les lettres de non objection
aux porteurs de projets ;
- approuver les résultats d’atténuation ;
- mettre en place le registre national des projets carbone
développés ;
- représenter l’Etat auprès des entités et promoteurs de
projets d’atténuation de gaz à effet de serre et projets
d’adaptation avec co-bénéfices en atténuation ;
- suivre l’évolution des règles, modalités et procédures
des standards internationaux de certification carbone ;
- gérer le processus d’autorisation de l’utilisation des
résultats d’atténuation transférés au niveau international
et des ajustements correspondants ;
- recevoir et gérer les plaintes et recours ainsi que leur
résolution.
Art. 7 : Le comité national d’homologation est placé sous la
tutelle du ministère chargé des changements climatiques.
Il est chargé de :
- assurer une mise en œuvre des projets conforme à la
législation nationale, aux objectifs de développement
durable du pays et à ses contributions déterminées au
niveau national ;
- vérifier les lignes de base et l’additionnalité des projets ;
- donner les avis techniques sur les demandes
d’homologation des projets et programmes d’atténuation
des émissions de gaz à effet de serre.
Art. 8 : Le comité national d’homologation est composé
comme suit :
- un (1) représentant du ministère chargé des changements
climatiques, président ;
- un (1) représentant du ministère chargé des énergies,
rapporteur ;
- un (1) représentant du ministère chargé des Finances,
membre ;
- un (1) représentant du ministère chargé de l’Agriculture,
membre ;
- un (1) représentant du ministère chargé de
l’Administration territoriale, membre ;
- un (1) représentant du ministère chargé de la
Planification du Développement, membre ;
- un (1) représentant du ministre chargé du foncier,
membre ;
- un (1) représentant du ministère chargé de l’Industrie,
membre ;
- un (1) représentant du ministère des Investissement,
membre ;
- un (1) représentant du ministère chargé des transports,
membre; un (1) représentant de la faitière des
communes du Togo, membre ;
- un (1) représentant du réseau des ONG intervenant dans
le domaine de l’environnement, membre.
Le comité national peut faire appel, à titre consultatif, à toute
personne dont les compétences sont jugées nécessaires
pour l’accomplissement de ses missions.
Les modalités de fonctionnement du comité national sont
précisées par arrêté conjoint du ministre chargé de
l’environnement et du ministre chargé des Finances.
Art. 9 : Le secrétariat technique de gestion des mécanismes
de carbone est assuré par la direction de l’environnement.
Le secrétariat technique de gestion des mécanismes de
carbone peut faire appel, à toute personne dont les
compétences sont jugées nécessaires pour
l’accomplissement de ses missions.
Art. 10 : Les missions du secrétariat technique de gestion
des mécanismes de carbone sont les suivantes :
- recevoir les demandes d’enregistrement de projets
carbone de la part des promoteurs de projets ;
- évaluer les projets et les programmes d’attenuation ;
- suivre la mise en œuvre des projets et programmes ;
- organiser les réunions du comité national d’homologation ;
- élaborer les rapports nationaux ;
- gérer le système d’information sur les sauvegardes socioenvironnementales ;
- recevoir les plaintes et recours ;
- formuler des propositions au comité national
d’homologation ;
- gérer le registre national des projets et des programmes.
CHAPITRE III : DES DROITS SUR LES RESULTATS
D’ATTENUATION
Art. 11 : Le droit de générer et de disposer des résultats
sous forme de crédit carbone, d’unité de réduction et de
paiements est reconnu aux personnes physiques ou morales.
Art. 12 : Les détenteurs de crédits carbones et des unités
de réduction peuvent librement les céder ou les transférer
par voie de convention.
Les opérations de cession et de transfert sont assujetties
au paiement d’une taxe dont les modalités de recouvrement
et de gestion sont fixées par arrêté conjoint du ministre
chargé des finances et du ministre chargé des changements
climatiques.
Art. 13 : Les résultats d’atténuation liés aux marchés régulés
ou volontaires font l’objet d’une retenue par l’autorité nationale
en vue de répondre aux engagements vis-à- vis des
contributions déterminées au niveau national.
Les modalités de la retenue sont fixées par arrêté du ministre
chargé des changements climatiques.
6 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
Le taux de la retenue est fonction de l’ampleur et de la nature
du projet ou du programme
CHAPITRE IV : DE L’HOMOLOGATION DES PROJETS
ET PROGRAMMES
Art. 14 : Pour être homologués, les projets et programmes
doivent répondre aux critères d’éligibilité ci-après :
- présenter des potentialités d’atténuation des émissions
de gaz à effet de serre et un potentiel de renforcement
des capacités et de transfert de technologie ;
- être accompagné d’un plan d’investissement cohérent ;
- être aligné sur les secteurs et les activités éligibles,
définis par l’arrêté prévu à l’article 2 du présent décret ;
- prévoir des mesures de sauvegarde environnementale et
sociale, y compris le genre ;
- présenter un dispositif de suivi-évaluation qui s’intègre
au système national de mesures, notification et
vérification.
Tout projet ou programme répondant aux critères ci-dessus
mentionnés est éligible et après le contrôle, donne droit à
une autorisation délivrée par l’autorité nationale.
Art. 15 : Aux fins d’application des dispositions de l’article 9
du présent décret, il est institué un registre national des
projets et programmes d’atténuation des émissions de gaz
à effet de serre, tenu et géré par le secrétariat technique.
Un arrêté du ministre chargé de l’environnement précise les
modalités de gestion du registre national des projets et
programmes d’atténuation des émissions de gaz à effet de
serre.
Art. 16 : Les pièces constitutives du dossier de demande et
les critères d’homologation sont définis par un manuel de
procédures approuvé par arrêté du ministre chargé des
changements climatiques.
CHAPITRE V: DES MECANISMES DE PARTAGE
DES BENEFICES ET DE GESTION DES PLAINTES
DANS LE CADRE DE REDD+
Art. 17 : Les paiements basés sur les résultats liés au
mécanisme REDD+ sont versés sur le fonds national de
l’environnement (FNE).
Les modalités de partage des paiements visés à l’alinéa
1
er du présent article sont déterminées par un arrêté
interministériel du ministre chargé des changements
climatiques, du ministre chargé de l’Administration
Territoriale et du ministre chargé des Finances.
Art. 18 : Les plaintes et les conflits liés à la mise en œuvre
des activités d’atténuation sont réglés conformément au
mécanisme de gestion des plaintes mis en place dans le
cadre de REDD+.
A défaut, le litige est soumis aux tribunaux togolais
compétents.
CHAPITRE VI : DES DISPOSITIONS FINALES
Art. 19 : Le ministre de l’Environnement et des Ressources
Forestières, le ministre d’Etat, ministre de l’Administration
Territoriale, de la Décentralisation et du Développement des
Territoires, le ministre du Commerce, de l’Industrie et de la
Consommation Locale, le ministre de l’Economie et des
Finances et le ministre délégué chargé de l’Energie et des
Mines sont chargés, chacun en ce qui le concerne, de
l’exécution du présent décret qui sera publié au Journal
Officiel de la République Togolaise.
Fait à Lomé, le 15 mars 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire S. TOMEGAH-DOGBE
Le ministre de l’Administration Territoriale, de la
Décentralisation et du Développement des Territoires
Payadowa BOUKPESSI
Le ministre de l’Environnement et des Ressources
 Forestières
Katari FOLI BAZI
Le ministre de l’Economie et des Finances
Sani YAYA
Le ministre du Commerce, de l’Industrie
et de la Consommation Locale
S- T Kodjo ADEDZE
Le ministre délégué chargé de l’Energie et des Mines
Mawunyo Mila AZIABLE
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 7
DECRET N° 2023-038 DU 05/04/2023
déclarant d’utilité publique et autorisant les travaux
de construction d’une centrale solaire photovoltaïque
à Dapaong
LE PRESIDENT DE LA REPUBLIQUE,
Sur rapport conjoint du ministre de l’Economie et des Finances, du
ministre délégué auprès du Président de la République, chargé des Mines
et de l’Energie et du ministre d’Etat, ministre de l’Administration Territoriale,
de la Décentralisation et du Développement des Territoires ;
Vu la constitution du 14 octobre 1992 ;
Vu la loi n° 2014-014 du 22 octobre 2014 portant modernisation de
l’action publique de l’Etat en faveur de l’économie ;
Vu la loi n° 2018-005 du 14 juin 2018 portant code foncier et domanial ;
Vu le décret n° 2012-004/PR du 29 février 2012 relatif aux attributions
des ministres d’Etat et ministres ;
Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
attributions du ministre et portant organisation et fonctionnement du
ministère de l’Economie et des Finances ;
Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
nomination du Premier ministre ;
Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
du Gouvernement, ensemble les textes qui l’ont modifié ;
Le conseil des ministres entendu,
DECRETE :
Article premier : Sont déclarés d’utilité publique et
autorisés, les travaux de construction d’une centrale solaire
photovoltaïque à Dapaong.
Art. 2 : Les travaux prévus à l’article 1er du présent décret
couvrent un site d’une contenance superficielle de soixantequatorze hectares soixante-six ares vingt-sept centiares
(74ha 66a 27ca), limité :
- au nord par le domaine de la collectivité BOUM ;
- au sud par les propriétés des collectivités LANGO et
YALBOUME ;
- à l’est par la collectivité YALBOUME ;
- et à l’ouest par le domaine de la collectivité LANGO.
Art. 3 : Le ministre de l’Economie et des Finances est
autorisé à prendre toutes les mesures relatives à la procédure
d’expropriation pour cause d’utilité publique.
Art. 4 : le ministre de l’Economie et des Finances, le ministre
délégué auprès du Président de la République, chargé des
mines et de l’Energie et le ministre d’Etat, ministre de
l’Administration Territoriale et du Développement des
Territoires sont chargés, chacun en ce qui le concerne, de
l’exécution du présent décret qui sera publié au Journal
Officiel de la République Togolaise.
Fait à Lomé, le 05 avril 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire S. TOMEGAH-DOGBE
Le ministre délégué auprès du Président
de la République, chargé de l’Energie et des Mines
Mawunyo Mila AZIABLE
Le ministre de l’Economie et des Finances
Sani YAYA
Le ministre d’Etat, ministre de l’Administration Territoriale,
de la Décentralisation et du Développement des
Territoires
Payadowa BOUKPESSI
_______
DECRET N° 2023-039/PR DU 05/04/2023
portant création, attributions et organisation
de la Société Togolaise de Manganèse (STM)
LE PRESIDENT DE LA REPUBLIQUE,
Sur le rapport conjoint de la ministre déléguée auprès du Président
de la République, chargée de l’Energie et des Mines et du ministre de
l’Economie et des Finances,
Vu la Constitution du 14 octobre 1992 ;
Vu la loi organique n° 2014-013 du 27 juin 2014 relative aux lois de
finances ;
Vu la loi n° 90-26 du 4 décembre 1990 portant réforme du cadre
institutionnel et juridique des entreprises publiques ;
Vu la loi n° 2003-012 du 4 octobre 2003 modifiant la loi n° 96-004 du
26 février 1996 portant code minier en République Togolaise ;
Vu la loi n° 2014-009 du 11 juin 2014 portant code de transparence
dans la gestion des finances publiques ;
Vu la loi n° 2014-014 du 22 octobre 2014 portant modernisation de
l’action de l’Etat en faveur de l’économie ;
8 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
Vu le décret n° 91-197 du 16 août 1991 pris pour l’application de la
loi n° 90-26 du 4 décembre 1990 portant réforme du cadre institutionnel
et juridique des entreprises publiques ;
Vu le décret n° 2012-004/PR du 29 février 2012 relatif aux attributions
des ministres d’Etat et ministres ;
Vu le décret n° 2012-006/PR du 7 mars 2012 portant organisation
des départements ministériels ;
Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
attributions du ministre et portant organisation et fonctionnement du
ministère de l’Economie et des Finances ;
Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
nomination du Premier ministre ;
Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
du Gouvernement, ensemble les textes qui l’ont modifié ;
Le conseil des ministres entendu,
DECRETE :
CHAPITRE IER : DE LA CREATION
ET DE L’ORGANISATION
Article premier : Il est créé une société d’Etat dénommée
« Société Togolaise de Manganèse », en abrégé « STM ».
La forme juridique de la STM est précisée dans les statuts
qui la régissent.
Art. 2 : La STM est constituée pour une durée de quatrevingt-dix-neuf (99) ans.
Art. 3 : La STM est régie par l’acte uniforme de l’organisation
pour l’harmonisation en Afrique du droit des affaires
(OHADA) relatif au droit des sociétés commerciales et du
groupement d’intérêt économique, et par la législation
nationale applicable aux entreprises publiques.
Art. 4 : La STM a pour objet l’exploitation, la valorisation et
la commercialisation du manganèse au Togo.
A ce titre, elle a notamment pour mission :
- la mise en valeur de tous gisements de manganèse sur
l’étendue du territoire ;
- la commercialisation du minerai de Manganèse et ses
produits finis ;
- l’implémentation d’usines de transformation de
manganèse au Togo ;
- la réalisation de toutes opérations industrielles,
commerciales, financières, mobilières ou immobilières
se rattachant directement à son objet social et à tout
objet similaire ou connexe.
Art. 5 : Le siège de la STM est fixé à Dapaong. Il peut être
transféré en tout autre lieu du territoire national sur décision
du gouvernement.
Art. 6 : Le capital social de la STM est fixé à deux cent
millions (200 000 000) de francs CFA, divisé en vingt mille
(20 000) actions d’une valeur nominale de dix mille (10 000)
francs CFA, entièrement souscrite par l’Etat.
L’augmentation du capital social pourra se faire
conformément aux dispositions législatives et réglementaires
en vigueur.
CHAPITRE II : DE L’ORGANISATION
ET DU FONCTIONNEMENT
Art. 7 : Le ministre chargé des mines assure la tutelle
technique et le ministre chargé des Finances assure la tutelle
financière de la société.
Art. 8 : Le ministre chargé des Mines donne des orientations
pour la définition de la stratégie de la société et s’assure de
la conformité des résolutions du conseil d’administration aux
lois et règlements en vigueur, ainsi qu’à la politique définie
par le gouvernement pour le secteur des industries
extractives et de transformation.
Le ministre chargé des Finances assure le suivi de la
performance de la STM, en collaboration avec le ministre
chargé des Mines.
Art. 9 : Le ministre chargé des Finances en collaboration
avec le ministre chargé des mines, s’assure de la régularité
des résolutions à incidence financière du conseil
d’administration et de la soutenabilité des engagements
financiers. Il veille à l’autorisation préalable des actes ayant
une incidence sur le patrimoine de la société et à sa
performance financière.
Art. 10 : La STM est dotée d’un conseil de surveillance dont
la composition, l’organisation et le fonctionnement sont fixées
conformément aux dispositions législatives et réglementaires
applicables aux entreprises publiques.
Art. 11 : Le conseil de surveillance a pour mission notamment
de :
- nommer, révoquer les administrateurs et fixer le montant
de leur indemnité de fonction ;
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 9
- nommer et révoquer les commissaires aux comptes ;
- conclure un contrat de performance avec le conseil
d’administration ;
- décider de l’affectation du résultat ;
- approuver le budget et les comptes de l’exercice et donner
quitus au conseil d’administration ;
- approuver les conventions conclues entre un
administrateur et la STM ;
- autoriser les contrats de travaux, de fourniture, de services
ou de gestion, lorsque le montant de ceux-ci excède le
seuil fixé par voie réglementaire ;
- adopter et modifier les statuts de la société, à l’exception
des modifications portant sur le nom, l’objet, la durée,
le montant du capital social, la consistance des apports
en nature, les organes de supervision, d’administration
et de gestion ainsi que les ministères de tutelle, la
procédure de dissolution et la dévolution de l’actif net
ou de toute autre mention jugée pertinente qui ne
peuvent se faire que par décret en conseil des ministres
sur rapport conjoint du ministre de tutelle technique et
du ministre chargé des Finances.
Art. 12 : La STM est administrée par un conseil
d’administration.
Le conseil d’administration est investi des pouvoirs
nécessaires pour agir au nom de la société et pour faire ou
autoriser toutes les opérations intéressant l’activité de ,la
STM dans les limites de son objet social.
A ce titre, le conseil d’administration est chargé de :
- instituer le directeur général et fixer le montant de sa
rémunération ;
- fixer les attributions du directeur général ;
- adopter le budget d’investissement et de fonctionnement ;
- adopter les comptes financiers et le rapport annuel
d’activité qu’il adresse au conseil de surveillance ;
- autoriser les conventions conclues entre la société et
l’un de ses administrateurs ou le directeur général et
son adjoint, le cas échéant ;
- adopter le statut du personnel et le règlement intérieur ;
- fixer le montant des engagements, des dépenses et les
organes habilités ;
- signer un contrat de performance avec la direction
générale.
Art. 13 : Le conseil d’administration est composé de trois
(3) membres au moins et de douze (12) membres au plus
dont :
- un (1) représentant du ministère chargé des Mines,
président ;
- un (1) représentant du ministère chargé des Finances,
vice-président.
Les statuts déterminent la composition et les règles de
fonctionnement du conseil d’administration.
Art. 14 : La STM est gérée par un directeur général recruté
sur la base d’un contrat. Le directeur général est nommé et
révoqué, conformément aux dispositions législatives et
réglementaires applicables aux entreprises publiques.
Le conseil d’administration fixe les attributions du directeur
général et sa rémunération conformément aux dispositions
législatives et réglementaires en vigueur.
Art. 15 : Les modalités de fonctionnement de la direction
générale sont précisées dans les statuts de la STM.
CHAPITRE III : DES DISPOSITIONS DIVERSES
ET FINALES
Art. 16 : La STM peut être dissoute pour les causes
prévues par les dispositions de l’acte uniforme de
l’organisation pour l’harmonisation en Afrique du droit des
affaires (OHADA) relatif au droit des sociétés commerciales
et du groupement d’intérêt économique, la législation
nationale applicable aux entreprises publiques et par ses
statuts.
Art. 17 : La dissolution de la STM est prononcée par décret
en conseil des ministres. En cas de dissolution, l’actif
net, restant après les opérations de liquidation, est dévolu à
l’Etat.
Art. 18 : Le ministre déléguée auprès du Président de la
République, chargé de l’Energie et des mines et le ministre
10 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
de l’Economie et des Finances sont chargés, chacun en ce
qui le concerne, de l’exécution du présent décret qui sera
publié au Journal Officiel de la République Togolaise.
Fait à Lomé, le 05 avril 2023
Le Président de la République
Faure Essozimna GNASSINGBE
Le Premier ministre
Victoire S. TOMEGAH-DOGBE
Le ministre de l’Economie et des Finances
Sani YAYA
Le ministre délégué auprès du Président de la République
chargé l’énergie et des mines
Mawunyo Mila AZIABLE
_______
ARRETES
ARRETE N° 211/2022/MEF/UPF DU 21/11/2022
portant création, attributions et organisation du
Comité national d’évaluation des dépenses fiscales
LE MINISTRE DE L’ECONOMIE ET DES FINANCES,
Vu la constitution du 14 octobre 1992 ;
Vu la directive n° 01/2009/CM/UEMOA du 27 mars 2009 portant
Code de Transparence dans la gestion des finances publiques au sein
de l’UEMOA ;
Vu la décision n° 08/2015/CM/UEMOA du 02 juillet 2015 instituant
les modalités d’évaluation des dépenses fiscales dans les Etats
membres de l’UEMOA ;
Vu la loi organique n° 2014-013 du 27 juin 2014 relative aux lois de
finances ;
Vu la loi n° 2014-014 du 22 octobre 2014 portant modernisation de
l’action publique de l’Etat en faveur de l’Economie ;
Vu la loi n° 2014-009 du 11 juin 2014 portant code de transparence
dans la gestion des finances publiques ;
Vu le décret n° 2012-006/PR du 07 mars 2012 portant organisation
des départements ministériels ;
Vu le décret n° 2017-112/PR du 29 septembre 2017 fixant les
attributions du ministre et portant organisation et fonctionnement du
ministère de l’Economie et des Finances ;
Vu le décret n° 2020-076/PR du 28 septembre 2020 portant
nomination du Premier Ministre ;
Vu le décret n° 2020-080/PR du 1er octobre 2020 portant composition
du gouvernement, ensemble les textes qui l’ont modifié ;
ARRETE :
Article premier : créé auprès du ministre de l’Economie et
des Finances un comité chargé de l’évaluation des dépenses
fiscales, dénommé « Comité national d’évaluation des
dépenses fiscales », en abrégé CONEDEF.
Art. 2 : le CONEDEF est chargé d’élaborer au plus tard le
30 septembre de chaque année, le rapport d’évaluation des
dépenses fiscales de l’exercice précédent, destiné à être
annexé au projet de loi de finances.
Art. 3 : le CONEDEF est chargé de :
- définir la méthodologie d’évaluation des dépenses
fiscales ;
- identifier les sources de données, recueillir les données,
organiser et rassembler les informations en vue de
l’élaboration du rapport d’évaluation des dépenses
fiscales ;
- élaborer le rapport annuel des dépenses fiscales ;
- déterminer le rapport coût/bénéfice des dépenses
fiscales et en mesurer l’efficacité ;
- proposer une rationalisation et une stratégie de réduction
des dépenses fiscales.
Art. 4 : le CONEDEF est composé comme suit :
Président :
- le directeur de l’Unité de politique fiscale.
Rapporteurs
- Deux (02) rapporteurs dont un (01) représentant de l’Unité
de politique fiscale (UPF) et un (01) représentant de l’Office
togolais des recettes (OTR).
Membres
- Quatre (04) représentants de l’Unité de politique fiscale
(UPF) ; Deux (02) représentants de l’Office togolais des
recettes (OTR) ;
- Un (01) représentant de la Direction générale des études
et analyses économiques (DGEAE) ;
- Un (01) représentant de l’Agence de promotion des
investissements et de la Zone franche (API-ZF) ;
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 11
- Un (01) représentant de l’Institut national des statistiques
et des études économiques et démographiques (INSEED) ;
- Deux (02) représentants du Comité national de suivi du
Programme de transition fiscale dont un (01) de la Cellule
CEDEAO-UEMOA.
Le Comité peut faire appel en tant que de besoin, à toute
personne dont la compétence est jugée nécessaire pour
l’accomplissement de sa mission.
Art. 5 : le CONEDEF se réunit périodiquement et
conformément au Plan de travail et budget annuel (PTBA)
validé afin de s’assurer de l’avancement des activités en vue
de l’élaboration du rapport d’évaluation des dépenses fiscales
dans les délais mentionnés à l’article 2 du présent arrêté.
A l’issue de chaque réunion, le Comité adresse un rapport
écrit au ministre des finances à l’effet de mesurer les progrès
accomplis et proposer des éventuels ajustements.
Art. 6 : les frais inhérents au fonctionnement du CONEDEF
sont pris en charge par le budget de l’Etat.
Art. 7 : Le secrétaire général du ministère de l’Economie et
des Finances est chargé de l’exécution du présent arrêté
qui prend effet à compter de sa date de signature et sera
publié au Journal Officiel de la République Togolaise.
Fait à Lomé, le 21 novembre 2022
Le ministre de l’Economie et des Finances
Sani YAYA
_______
ARRET N°008/2023 DU 25/05/2023
RECOURS N°004/R.EL/2023 du 24 mars 2023
AFFAIRE : Le préfet de Tône (Tône 1) C/ QUID DE
DROIT
PRESENTS : MM DJIDONOU : PRESIDENT HOUSSIN
ASSAH ZEKPA GBADOE MEMBRES DODZRO : M.P.
DORSOU : GREFFIERE
« AU NOM DU PEUPLE TOGOLAIS »
AUDIENCE PUBLIQUE ORDINAIRE DU JEUDI VINGT
CINQ MAI DEUX MILLE VINGT TROIS (25/05/2023)
ARRET DE DESIGNATION DE CONSEILLER
MUNICIPAL
A l’audience publique ordinaire de la chambre administrative
de la Cour suprême, tenue le vingt-cinq mai deux mille vingttrois, est intervenu l’arrêt suivant :
LA COUR
Suivant requête n° 001/2023-MATDDT/RS/PT du 16 mars
2023 enregistrée au greffe de la Cour suprême le 24 mars
2023, le préfet de Tône a saisi la chambre administrative de
la Cour suprême aux fins de constatation de l’indisponibilité
définitive du conseiller municipal SAMBANE DJILA Yalbondja
du parti politique Union pour la République (UNIR) dans la
commune de Tône 1 pour cause de décès et a sollicité la
désignation d’un conseiller pour compléter la liste du parti
UNIR dans ladite commune ;
Vu la déclaration de décès n° 31 du 21 février 2023 du maire
de la commune de Tône 1 ;
Vu les autres pièces du dossier ;
Vu la constitution du 14 octobre 1992 ensemble les textes
modificatifs ;
Vu la loi organique n° 97-05 du 06 mars 1997 portant
organisation et fonctionnement de la Cour suprême ;
Vu la loi n° 2022-011 du 4 juillet 2022 portant modification
de la loi n° 2007-011 du 13 mars 2007 relative à la
décentralisation et aux libertés locales modifiée par la loi
n° 2018-003 du 31 janvier 2018, la loi n° 2019-006 du 26 juin
2019 et la loi n° 2021-020 du 11 octobre 2021 ;
Vu la loi n° 2022-007 du 30 mai 2022 ponant modification de
la loi n° 2012-002 du 29 mai 2012 portant code électoral
modifiée par la loi n° 2013-004 du 19 février 2013, la loi
n° 2013-008 du 22 mars 2013, la loi n° 2019-017 du
6 novembre 2019 et la loi n° 2021-019 du 11 octobre 2021 ;
Vu la loi n° 91-04 du 12 avril 1991 portant charte des partis
politiques ;
Vu le décret n° 2019-070/PR du 07 mai 2019 fixant la date
des élections des conseillers municipaux et convoquant le
corps électoral pour les élections des conseillers municipaux
du 30 juin 2019 ;
Vu l’arrêt n° 45/2019 du 17 juillet 2019 portant proclamation
des résultats définitifs des élections municipales du 30 juin
2019 ;
Vu le décret n° 2019/100/PR du 24 juillet 2019 fixant la date
des élections partielles des conseillers municipaux des
communes de OTI-SUD 1, BASSAR 4, WAWA 1, ZIO 4,
AVE 2 et convoquant le corps électoral des conseillers
municipaux du 15 août 2019 ;
12 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
Vu l’arrêt n° 52/2019 du 30 août 2019 portant proclamation
des résultats définitifs des élections partielles du 15 août
2014 ;
Après avoir entendu à l’audience publique :
le rapport de madame Akpénè DJIDONOU, présidente de la
chambre administrative de la Cour suprême ;
les conclusions de monsieur DODZRO Komlan, cinquième
avocat général près la Cour suprême ;
Considérant que de la requête aux fins de constatation de
vacance de poste transmise à la Cour par monsieur le préfet
de Tône, il ressort que le conseiller municipal SAMBANE
DJILA Yalbondja du parti politique UNIR de la commune de
Tône 1 est décédé ;
Considérant qu’il est établi que le conseiller municipal
SAMBANE DJILA Yalbondja du parti politique UNIR de la
commune de Tône 1 est décédé le 10 février 2023 ainsi que
l’atteste la déclaration de décès n° 31 du 21 février 2023 du
maire de la commune de Tône 1 ; qu’il échet d’en prendre
acte, de déclarer son siège vacant et d’indiquer le nom de
son remplaçant ;
Considérant que l’article 274 al 3 du code électoral
dispose : « en cas de démission, de décès ou d’acceptation
d’une fonction déclarée incompatible avec la fonction de
conseiller municipal, les sièges vacants sont occupés selon
l’ordre de présentation aux électeurs » ; qu’il en résulte que
la détermination de la personne habilitée à occuper un siège
vacant doit tenir compte de l’ordre des présentations, des
candidats sur la liste du parti politique de la commune
concernée ; qu’ainsi dans la commune de Tône 1, onze (11)
conseillers étant élus sur la liste du parti politique UNIR,
monsieur SAMBANE DJILA Yalbondja sera remplacé par
monsieur DJARGUI Gnimpal, de sexe masculin, né le 24
avril 1975 à Dapaong, enseignant, demeurant et domicilié à
Dapaong, 12e
 sur la liste ;
DECIDE
Article premier : Prend acte du décès de SAMBANE DJILA
Yalbondja, conseiller municipal sur la liste du parti
politique Union pour la République (UNIR) dans la commune
de Tône 1 ;
Art. 2 : Constate la vacance du siège précédemment occupé
par le défunt ;
Art. 3 : Désigne monsieur DJARGUI Gnimpal, de sexe
masculin, né le 24 avril 1975 à Dapaong, enseignant,
demeurant et domicilié à Dapaong, candidat suivant
(douzième) sur la liste du parti politique Union pour la
République (UNIR) aux élections municipales du 30 juin 2019
dans la commune de Tône 1, pour le remplacer ;
Art. 4 : Ordonne la publication de la présente décision au
Journal Officiel de la République Togolaise selon la procédure
d’urgence ;
Délibérée par la Cour en son audience publique ordinaire du
25 mai 2023 à laquelle siégeaient :
Madame Akpénè DJIDONOU, présidente de la chambre
administrative, présidente ;
Messieurs HOUSSIN Kossi, ASSAH Kindbelle Yvetus,
madame ZEKPA Apoka Madjé et Monsieur GBADOE Edoh
Dodji tous quatre, conseillers à la chambre administrative
de la Cour suprême, membres ;
En présence de monsieur DODZRO Komlan, cinquième
avocat général près la Cour suprême ;
Et avec l’assistance de maître DORSOU Essi Djigbodi,
greffière à la Cour suprême, greffière ;
En foi de quoi, le présent arrêt a été signé par la
présidente et la greffière.
________
ARRET N°009/2023 DU 25/05/2023
RECOURS N°005/R.EL/2023 du 25 avril 2023
AFFAIRE : Le préfet de Haho (Haho 3) C/ QUID DE
DROIT
PRESENTS: MM DJIDONOU : PRESIDENT HOUSSIN
ASSAH ZEKPA GBADOE : MEMBRES AZANLEDJIAHADZI : M.P. DORSOU : GREFFIERE
« AU NOM DU PEUPLE TOGOLAIS »
AUDIENCE PUBLIQUE ORDINAIRE DU JEUDI VINGT
CINQ MAI DEUX MILLE VINGT TROIS (25/05/2023)
ARRET DE DESIGNATION DU REMPLACANT D’UN
CONSEILLER MUNICIPAL DEMISSIONNAIRE
A l’audience publique ordinaire de la chambre administrative
de la Cour suprême, tenue le vingt-cinq mai deux mille vingttrois, est intervenu l’arrêt suivant :
LA COUR
Vu la requête n° 005/MATDDT/RP/PH 2023 du 6 février 2023
enregistrée au greffe de la Cour suprême le 25 avril 2023 par
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 13
laquelle le préfet de Haho a transmis à la chambre
administrative la lettre de démission de monsieur KONDO
Kpapou, conseiller municipal du parti politique Union pour
la République (UNIR) dans la commune de Haho 3 et a
sollicité son remplacement afin de compléter l’effectif du
conseil municipal ;
Vu la lettre de démission en date du 22 août 2022 du
conseiller municipal KONDO Kpapou ;
Vu les autres pièces du dossier ;
Vu la constitution du 14 octobre 1992 ensemble les textes
modificatifs ;
Vu la loi organique n° 97-05 du 06 mars 1997 portant
organisation et fonctionnement de la Cour suprême ;
Vu la loi n° 2022-011 du 4 juillet 2022 portant modification
de la loi n° 2007-011 du 13 mars 2007 relative à la
décentralisation et aux libertés locales modifiée par la loi
n° 2018-003 du 31 janvier 2018, la loi n° 2019-006 du 26 juin
2019 et la loi n° 2021-020 du 11 octobre 2021 ;
Vu la loi n° 2012-002 du 29 mai 2012 portant code électoral
modifiée par la loi n° 2013-004 du 19 février 2013, la loi
n° 2013-008 du 22 mars 2013, la loi n° 2019-017 du
6 novembre 2019 et la loi n° 2021-019 du 11 octobre 2021 ;
Vu la loi n° 91-04 du 12 avril 1991 portant charte des partis
politiques ;
Vu le décret n° 2018-029/PR du 1er février 2018 précisant le
nombre de conseillers et le nombre d’adjoints au maire par
commune ;
Vu le décret n° 2019-070/PR du 07 mai 2019 fixant la date
des élections des conseillers municipaux et convoquant le
corps électoral pour les élections des conseillers municipaux
du 30 juin 2019 ;
Vu l’arrêt n° 45/2019 du 17 juillet 2019 portant proclamation
des résultats définitifs des élections municipales du 30 juin
2019 ;
Vu le décret n° 2019/100/PR du 24 juillet 2019 fixant la date
des élections partielles des conseillers municipaux des
communes de OTI-SUD 1, BASSAR 4, WAWA 1, ZIO 4,
AVE 2 et convoquant le corps électoral des conseillers
municipaux du 15 août 2019 ;
Vu l’arrêt n° 52/2019 du 30 août 2019 portant proclamation
des résultats définitifs des élections partielles du 15 août
2014 ;
Après avoir entendu à l’audience publique :
le rapport de madame DJIDONOU Akpénè, présidente de la
chambre administrative de la Cour suprême ;
les conclusions de madame AZANLEDJI-AHADZI M. Justine,
procureur général près la Cour suprême ;
Considérant que de la lettre de démission transmise à la
Cour par monsieur le préfet de Haho, il ressort qu’un
conseiller du parti UNIR de la commune de Haho 3 en la
personne de monsieur KONDO Kpapou a démissionné de
son mandat pour des raisons d’incompatibilité du poste de
secrétaire général de la préfecture de l’Ogou qu’il occupe
actuellement ;
Considérant que l’article 113 de la loi n° 2022-011 du 4 juillet
2022 portant modification de la loi n° 2007-011 du 13 mars
2007 relative à la décentralisation et aux libertés locales
modifiée par la loi n° 2018-003 du 31 janvier 2018, la loi
n° 2019-006 du 26 juin, 2019 et la loi n° 2021-020 du 11
octobre 2021 dispose: « Tout membre du conseil municipal
peut démissionner de ses fonctions. La démission est
adressée par écrit au maire qui doit accuser réception.
Le maire en informe le préfet. Il en informe également le
conseil municipal à sa prochaine séance.
La démission entre en vigueur à compter de la date de
constatation de cette démission par la juridiction compétente
sur saisine du préfet... » ;
Considérant qu’il est établi que le conseiller KONDO Kpapou
du parti UNIR de la commune de Haho 3 a déposé sa
démission le 22 août 2022 ; qu’il échet d’en prendre acte,
de déclarer son siège vacant et d’indiquer le nom de son
remplaçant ;
Considérant que l’article 274 al 3 du code électoral
dispose : « En cas de démission, de décès ou d’acceptation
d’une fonction déclarée incompatible avec la fonction de
conseiller municipal, les sièges vacants sont occupés selon
l’ordre de présentation aux électeurs » ; qu’il en résulte que
la détermination de la personne habilitée à occuper un siège
vacant doit tenir compte de l’ordre des présentations des
14 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 25 Mai 2023
candidats sur la liste du parti politique de la commune
concernée ; qu’ainsi dans la commune de Haho 3, neuf (09)
conseillers étant élus sur la liste UNIR, monsieur KONDO
Kpapou sera remplacé par madame KOMOU Tchaa
Pidénam, de sexe féminin, née le 15 février 1985 à PagalaLassa (P/Blitta), agent de promotion sociale, demeurant et
domiciliée à Notsé, dixième sur ladite liste ;
DECIDE
Article premier : Prend acte de la démission de monsieur
KONDO Kpapou, deuxième sur la liste UNIR de la commune
de Haho 3 ;
Art. 2 : Constate la vacance du siège précédemment occupé
par le conseiller démissionnaire ;
Art. 3 : Dit que le siège vacant sera occupé par madame
KOMOU Tchaa Pidénam, de sexe féminin, née le 15 février
1985 à Pagala-Lassa (P/Blitta), agent de promotion sociale,
demeurant et domiciliée à Notsé, candidate suivante
(dixième) sur la liste du parti politique Union pour la
République (UNIR) de la commune de Haho 3 ;
Imp. Editogo
Dépôt légal n° 39 bis
Art. 4 : Ordonne la publication de la présente décision au
Journal Officiel de la République Togolaise selon la procédure
d’urgence ;
Délibérée par la Cour en son audience publique ordinaire du
25 mai 2023 à laquelle siégeaient :
Madame DJIDONOU Akpénè, présidente de la chambre
administrative, présidente ;
Messieurs HOUSSIN Kossi, ASSAH Kindbelle Yvetus,
madame ZEKPA Apoka Madjé et monsieur GBADOE Edoh
Dodji, tous quatre, conseillers à la chambre administrative
de la Cour suprême, membres ;
En présence de madame AZANLEDJI-AHADZI M. Justine,
procureur général près la Cour suprême ;
Et avec l’assistance de maître DORSOU Essi Djigbodi,
greffière à la Cour suprême, greffière ;
En foi de quoi, le présent arrêt a été signé par la présidente
et la greffière.
25 Mai 2023 JOURNAL OFFICIEL DE LA REPUBLIQUE TOGOLAISE 15`;

// decret = "DECRET N° 234-234 DU 25/07/2000 LE PRESIDENT DE LA REPUBLIQUE, Sur rapport conjoint Du;;DECRETE:FaitàLoméLePremierministre_"
for (const decret of decrets) {
    const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar));
    parser.feed(decret)
    writeFile("./decret.json", JSON.stringify(parser.results), "utf-8", ()=> {
        console.log("Saved to fs")
    });
}