import * as moo from "moo";
export function getMessage(): string {
  return 'Hello world!';
}

export function createLexer(str: string) {
  const lexer = moo.compile({
    white_space: /[ \t]+/,
    slash:/\//,
    hiphen:/-/,
    long_hiphen:/—/,
    comma:/,/,
    colon:/:/,
    semicolon:/;/,
    dot:/\./,
    backslash:/\\/,
    underscore:/_/,
    plus:'+',
    equals:'=',
    lparen:/\{|\(|\[|«/,
    rparen:/\}|\)|\]|»/,
    number: [
      {
        match: /[1-9][0-9]*/,
        value: x => {
          let n:any = parseInt(x)
          return x
        }
      }
    ],
    no_acronym:/N°|n°/,
    word: /[\wÀ-Üà-øoù-ÿŒœ’']+/,
    newline: { match: /\n/, lineBreaks: true },
  })

  lexer.reset(str);
  return lexer
}